<?php

namespace App\Http\Controllers;

use App\Mail\ApprovalLembur;
use Illuminate\Http\Request;
use App\Models\Lembur;
use App\Models\User;
use Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use Whoops\Run;

class LemburController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::allows('admin')) {
            $data = [
                'title' => 'Data Lembur',
                'badge' => 'Data Lembur',
                'active' => 'data_lembur',
                'lemburs' => Lembur::join('users', 'lemburs.user_id', '=', 'users.id')
                    ->select('lemburs.*', 'users.name', 'users.nip')
                    ->latest()
                    ->get()
            ];
        } else {
            $data = [
                'title' => 'Data Lembur',
                'badge' => 'Data Lembur',
                'active' => 'data_lembur',
                'lemburs' => Lembur::latest()->where('user_id', auth()->user()->id)
            ];
        }
        return view('lemburs.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Aju Lembur',
            'badge' => 'Aju Lembur',
            'active' => 'aju_lembur'
        ];
        return view('lemburs.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::allows('admin')) {
            $validatedData = $request->validate([
                'nip' => 'required',
                'alasan' => 'required|max:500'
            ]);

            $validatedData['user_id'] = User::where('nip', $validatedData['nip'])->value('id');
        } else {
            $validatedData = $request->validate([
                'alasan' => 'required|max:500'
            ]);

            $validatedData['user_id'] = auth()->user()->id;
        }

        $validatedData['status'] = 'diproses';
        $validatedData['code'] = Str::random(9) . strval($request['user_id']);

        Lembur::create($validatedData);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Lembur $lembur)
    {
        $data = [
            'title' => 'Lihat Data',
            'badge' => 'Lihat Data',
            'active' => 'data_lembur',
            'lemburs' => Lembur::join('users', 'lemburs.user_id', '=', 'users.id')
                ->select('lemburs.*', 'users.name', 'users.nip')
                ->where('code', $lembur->code)
                ->firstOrFail()
        ];

        dd(response()->json(Lembur::join('users', 'lemburs.user_id', '=', 'users.id')
            ->select('lemburs.*', 'users.name', 'users.nip')
            ->where('code', $lembur->code)
            ->firstOrFail()));

        return view('lemburs.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Lembur $lembur)
    {
        $data = [
            'title' => 'Edit Data',
            'badge' => 'Edit Data',
            'active' => 'data_lembur',
            'lemburs' => Lembur::join('users', 'lemburs.user_id', '=', 'users.id')
                ->select('lemburs.*', 'users.name', 'users.nip')
                ->where('code', $lembur->code)
                ->firstOrFail()
        ];

        return view('lemburs.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lembur $lembur)
    {
        $validateData = $request->validate([
            'alasan' => 'required|max:500'
        ]);

        $lembur = Lembur::where('code', $lembur->code)->firstOrFail();
        $lembur->alasan = $validateData['alasan'];
        $lembur->save();

        return redirect('/lemburs')->with('success', 'Data berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($code)
    {
        $lembur = Lembur::where('code', $code)->delete();

        return redirect('/lemburs')->with('success', 'Data berhasil dihapus!');
    }

    public function approve($code)
    {
        $lembur = Lembur::where('code', $code)->firstOrFail();
        $lembur->status = 'disetujui';
        $lembur->save();

        $email = User::where('id', $lembur->user_id)->firstOrFail()->value('email');

        Mail::to($email)->send(new ApprovalLembur($lembur));

        return redirect('/lemburs')->with('success', 'Lembur Disetujui!');
    }

    public function reject(Request $request, $code)
    {
        $validateData = $request->validate([
            'keterangan' => 'required|max:255'
        ]);

        $lembur = Lembur::where('code', $code)->firstOrFail();
        $lembur->keterangan = $validateData['keterangan'];
        $lembur->status = 'ditolak';
        $lembur->save();

        $email = User::where('id', $lembur->user_id)->firstOrFail()->value('email');

        Mail::to($email)->send(new ApprovalLembur($lembur));

        return redirect('/lemburs')->with('success', 'Lembur Tidak Disetujui!');
    }

    public function autocomplete(Request $request)
    {
        $data = User::select('nip')
            ->where('nip', 'LIKE', '%' . $request->term . '%')
            ->get();

        return response()->json($data);
    }
}
