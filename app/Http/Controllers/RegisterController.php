<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function create()
    {
        return view('register');
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:255',
            'nip' => 'required|max:18',
            'email' => 'required|email:dns|unique:users',
            'password' => 'required|min:5|max:255|confirmed',
        ]);

        $validateData['password'] = bcrypt($validateData['password']);
        $validateData['role'] = 'user';

        User::create($validateData);

        return redirect('/')->with('success', 'Register berhasil!');
    }
}
