<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lembur extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function scopeGetLemburById($query, $id)
    {
        $data = $query->join('users', 'lemburs.user_id', '=', 'users.id')
            ->select('users.name', 'users.nip', 'lemburs.created_at', 'lemburs.alasan')
            ->where('lemburs.user_id', $id)
            ->get();

        return $data;
    }

    public function getRouteKeyName()
    {
        return 'code';
    }
}
