<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::create([
            'name' => 'Sukuna',
            'nip' => '9987654543123',
            'email' => 'sukuna@gmail.com',
            'password' => bcrypt('qwerty')
        ]);

        User::create([
            'name' => 'Henny Febrianti',
            'nip' => '99876545434567',
            'email' => 'henny@gmail.com',
            'password' => bcrypt('qwerty')
        ]);

        User::create([
            'name' => 'Arizli Romadhon',
            'nip' => '9987654543333',
            'email' => 'arizli@gmail.com',
            'password' => bcrypt('qwerty')
        ]);
    }
}
