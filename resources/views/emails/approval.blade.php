<div>
    Status lembur Anda
    <br>
    Tanggal: {{ date('d-m-Y', strtotime($approval->created_at)) }}
    <br>
    Isi: {{ $approval->alasan }}
    <br>
    Status: <strong>{{ $approval->status }}</strong>
    <br>
    @if ($approval->keterangan != null)
    Keterangan: {{ $approval->keterangan }}
    @endif
</div>