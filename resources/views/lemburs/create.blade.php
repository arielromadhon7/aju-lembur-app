@extends('layouts.main_user')

@section('main')
    <div class="container">

        
        <div class="alert bg-success text-white" role="alert" id="success-alert" style="display: none">
            Berhasil mengajukan lembur
        </div>
        
        @can('admin')
        <form role="form" name="autocomplete-textbox" id="autocomplete-textbox">
            @csrf
            
            <div class="mb-3">
                <label>NIP</label>
                <div class="mb-3">
                    <input type="text" id="nip" class="typeahead form-control @error('nip') is-invalid @enderror" placeholder="NIP" name="nip" required>
                    @error('nip')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <label>Alasan Lembur</label>
                <textarea id="alasan" class="form-control @error('alasan') is-invalid @enderror" name="alasan" id="alasan" rows="5" required></textarea>
                @error('alasan')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="text-start">
                <button type="button" id="btn-save" class="btn bg-gradient-info mt-4 mb-0">Ajukan</button>
            </div>
        </form>
        @else
        <div class="card">
            <div class="card-body p-3">
                    <label>Tanggal (d/m/y)</label>
                    <div class="mb-3">
                        <input type="text" class="form-control" value="{{ date('d-F-Y') }}" readonly>
                    </div>
                    <label>NIP</label>
                    <div class="mb-3">
                        <input type="text" class="form-control" placeholder="NIP" value="{{ auth()->user()->nip }}" readonly>
                    </div>
                    <label>Nama Lengkap</label>
                    <div class="mb-3">
                        <input type="text" class="form-control" placeholder="Nama Lengkap" value="{{ auth()->user()->name }}" readonly>
                    </div>
                <form role="form">
                    @csrf
                    <label>Alasan Lembur</label>
                    <div class="mb-3">
                        <textarea id="alasan" class="form-control @error('alasan') is-invalid @enderror" name="alasan" id="alasan" rows="5" required></textarea>
                        @error('alasan')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="text-start">
                        <button type="button" id="btn-save" class="btn bg-gradient-info mt-4 mb-0">Ajukan</button>
                    </div>
                </form>
            </div>
        </div>
        @endcan

        
    </div>
@endsection

@section('ajax')

    <script>
        $(document).ready(function () {
            $("#nip").autocomplete({
                
                source: function(request, response) {
                    $.ajax({
                    url: "{{ route('autocomplete') }}",
                    type: "GET",
                    data: {
                        term : request.term
                    },
                    dataType: "json",
                    success: function(data){
                        let resp = $.map(data,function(value){
                            return value.nip;
                        }); 

                        response(resp);
                    }
                });
                },
                minLength: 2
            });


            $('#btn-save').click(function (e) {
                let alasan = $('#alasan').val();
                let token = $('input[name=_token]').val();
                let nip;
                let data;
                
                if($('#nip').length){
                    nip = $('#nip').val();
                    data = {
                        alasan : alasan,
                        nip : nip,
                        _token : token
                    }
                }else{
                    data = {
                        alasan : alasan,
                        _token : token
                    }
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: '{{ url("/lemburs") }}',
                    type: 'POST',
                    data: data,
                    success: function(data){
                        $('#success-alert').css('display', 'block').fadeOut(5000);
                    }
                });            
            });
        });
    </script>
@endsection