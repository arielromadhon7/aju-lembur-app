@extends('layouts.main_user')

@section('main')
    <div class="container">
        
        <div class="card">
            <div class="card-body p-3">
                    <label>Tanggal (d/m/y)</label>
                    <div class="mb-3">
                        <input type="text" class="form-control" value="{{  date('d-m-Y', strtotime($lemburs->created_at)) }}" readonly>
                    </div>
                    <label>NIP</label>
                    <div class="mb-3">
                        <input type="text" class="form-control" placeholder="NIP" value="{{ $lemburs->nip }}" readonly>
                    </div>
                    <label>Nama Lengkap</label>
                    <div class="mb-3">
                        <input type="text" class="form-control" placeholder="Nama Lengkap" value="{{ $lemburs->name }}" readonly>
                    </div>
                <form role="form" action="/lemburs/{{ $lemburs->code }}" method="POST">
                    @method('put')
                    @csrf
                    <label>Alasan Lembur</label>
                    <div class="mb-3">
                        <textarea class="form-control @error('alasan') is-invalid @enderror" name="alasan" id="" rows="5" required>{{ $lemburs->alasan }}</textarea>
                        @error('alasan')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="text-start">
                        <button type="submit" class="btn bg-gradient-info mt-4 mb-0">Edit</button>
                    </div>
                </form>
                <div class="text-start">
                    <a href="/lemburs">
                        <button class="btn bg-gradient-info mt-4 mb-0">Kembali</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection