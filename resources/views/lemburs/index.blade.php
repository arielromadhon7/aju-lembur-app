@extends('layouts.main_user')

@section('main')
<div class="row">
    <div class="col-12">

      @if (session()->has('success'))
        <div class="alert bg-success alert-dismissible fade show text-white" role="alert">
            {{ session('success') }}
            <button type="button" class="btn-close text-white" data-bs-dismiss="alert" aria-label="Close">&times;</button>
        </div>
        @endif

      <div class="card mb-4">
        <div class="card-header pb-0">
          <h6>Tabel Data Lembur</h6>
          <hr>
          <p class="text-sm font-weight-bold mb-0">{{ auth()->user()->name }}</p>
          <p class="text-sm text-secondary mb-0">{{ auth()->user()->nip }}</p>
        </div>
        <div class="card-body px-0 pt-0 pb-2">
          <div class="table-responsive p-3">
            <table class="table align-items-center justify-content-center mb-0 table-borderless" id="myTable">

              @can('admin')
              <thead>
                <tr>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">No</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Nama</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">NIP</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Tanggal</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Alasan</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Status</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Keterangan</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder text-center opacity-7 ps-2">Aksi</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder text-center opacity-7 ps-2">Aproval</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($lemburs as $lembur)
                <tr>
                  <td class="text-secondary text-xs font-weight-bold">{{ $loop->iteration }}</td>
                  <td class="text-secondary text-xs font-weight-bold">{{ $lembur->name }}</td>
                  <td class="text-secondary text-xs font-weight-bold">{{ $lembur->nip }}</td>
                  <td class="text-secondary text-xs font-weight-bold">{{ date('d-m-Y', strtotime($lembur->created_at)) }}</td>
                  <td class="text-secondary text-xs font-weight-bold">{{ Str::limit($lembur->alasan, 40, '...') }}</td>
                  <td class="badge m-2 text-xxs rounded-pill text-capitalize {{ $lembur->status === 'disetujui' ?'bg-success' : ($lembur->status === 'ditolak' ? 'bg-danger' : 'bg-info') }}">{{ $lembur->status }}</td>
                  <td class="text-secondary text-xs font-weight-bold">{{ $lembur->keterangan }}</td>
                  <td class="text-center">
                    <div class="d-flex justify-content-center">
                      <a href="/lemburs/{{ $lembur->code }}" class="text-info me-2" data-bs-toggle="tooltip" data-bs-placement="top" title="View"><i class="bi bi-eye-fill"></i></a>
                      <a href="/lemburs/{{ $lembur->code }}/edit" class="text-warning me-2" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="bi bi-pencil-square"></i></a>
                      <button id="btn-modal" type="button" class="border-0 bg-transparent text-danger"  data-bs-toggle="modal" data-bs-target="#deleteModal{{ $lembur->code }}"><i class="bi bi-trash-fill"></i></button>
                    
                      <!-- Delete Confirmation -->
                      <div class="modal fade" id="deleteModal{{ $lembur->code }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="deleteModalLabel">Hapus</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                              Apa Anda yakin ingin menghapus data di nomor {{ $loop->index + 1 }}?
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="border-0 bg-transparent pb-2 text-info" data-bs-dismiss="modal">Tidak, kembali</button>
                              <form action="/lemburs/{{ $lembur->code }}" method="POST">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger">Hapus</button>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </td>
                  <td class="text-secondary text-sm font-weight-bold">
                    <a href="/lemburs/{{ $lembur->code }}/approve" class="text-success me-2" data-bs-toggle="tooltip" data-bs-placement="top" title="Setuju"><i class="bi bi-check-circle"></i></a>
                    <button id="btn-modal" type="button" class="border-0 bg-transparent text-danger"  data-bs-toggle="modal" data-bs-target="#rejectModal{{ $lembur->code }}"><i class="bi bi-x-circle"></i></button>
                  
                    <!-- Reject Detail -->
                    <div class="modal fade" id="rejectModal{{ $lembur->code }}" tabindex="-1" role="dialog" aria-labelledby="rejectModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title text-xs" id="rejectModalLabel">Tidak Setujui Lembur {{ $lembur->name }} tanggal {{ date('d-m-Y', strtotime($lembur->created_at)) }}</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                          </div>
                          <div class="modal-body">
                            <form action="/lemburs/{{ $lembur->code }}/reject" method="POST">
                              @csrf
                              @method('put')
                              <label>Keterangan</label>
                              <div class="mb-3">
                                  <input type="text" class="form-control" placeholder="Keterangan" name="keterangan" required>
                              </div>
                              <div class="text-start">
                                <button type="button" class="btn bg-transparent mt-4 mb-0" data-bs-dismiss="modal">kembali</button>
                                <button type="submit" class="btn bg-gradient-info mt-4 mb-0">Kirim</button>
                            </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>

                  </td>
                </tr>
                @endforeach
              </tbody>

              @else
              <thead>
                <tr>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">No</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Tanggal</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Alasan</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Status</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Keterangan</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder text-center opacity-7 ps-2">Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($lemburs as $lembur)
                <tr>
                  <td class="text-secondary text-xs font-weight-bold">{{ $loop->index + 1 }}</td>
                  <td class="text-secondary text-xs font-weight-bold">{{ date('d-m-Y', strtotime($lembur->created_at)) }}</td>
                  <td class="text-secondary text-xs font-weight-bold">{{ Str::limit($lembur->alasan, 40, '...') }}</td>
                  <td class="badge m-2 text-xxs rounded-pill text-capitalize {{ $lembur->status == 'disetujui' ? 'bg-success' : ($lembur->status == 'ditolak' ? 'bg-danger' : 'bg-info') }}">{{ $lembur->status }}</td>
                  <td class="text-secondary text-xs font-weight-bold">{{ $lembur->keterangan }}</td>
                  <td class="text-center">
                    <div class="d-flex justify-content-center">
                      @if ($lembur->status == 'diproses')
                      <a href="/lemburs/{{ $lembur->code }}" class="text-info me-2" data-bs-toggle="tooltip" data-bs-placement="top" title="View"><i class="bi bi-eye-fill"></i></a>
                      <a href="/lemburs/{{ $lembur->code }}/edit" class="text-warning me-2" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="bi bi-pencil-square"></i></a>
                      <button id="btn-modal" type="button" class="border-0 bg-transparent text-danger"  data-bs-toggle="modal" data-bs-target="#deleteModal{{ $lembur->code }}"><i class="bi bi-trash-fill"></i></button>
                    
                      <!-- Modal -->
                      <div class="modal fade" id="deleteModal{{ $lembur->code }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="deleteModalLabel">Hapus</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                              Apa Anda yakin ingin menghapus data di nomor {{ $loop->index + 1 }}?
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="border-0 bg-transparent pb-2 text-info" data-bs-dismiss="modal">Tidak, kembali</button>
                              <form action="/lemburs/{{ $lembur->code }}" method="POST">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger">Hapus</button>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>

                      @else

                      <a href="/lemburs/{{ $lembur->code }}" class="text-info me-2" data-bs-toggle="tooltip" data-bs-placement="top" title="View"><i class="bi bi-eye-fill"></i></a>
                      
                      @endif
                      

                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>

              @endcan
              


            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  
@endsection

@section('ajax')
<script>
  $(document).ready( function () {
      $('#myTable').DataTable();
  } );
</script>
@endsection