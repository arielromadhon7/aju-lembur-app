<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 " id="sidenav-main">
    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
        <a class="navbar-brand m-0" href="/">
            <span class="mx-auto font-weight-bold fs-4">Aju Lembur App</span>
        </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse  w-auto  max-height-vh-100 h-100" id="sidenav-collapse-main">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link  {{ Request::is('welcome') ? 'active' : '' }}" href="/welcome">
                    <i class="bi bi-house-door-fill"></i>
                    <span class="nav-link-text ms-1">Home</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link  {{ Request::is('lemburs/create') ? 'active' : '' }}" href="/lemburs/create">
                    <i class="bi bi-file-earmark-medical-fill"></i>
                    <span class="nav-link-text ms-1">Aju Lembur</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link  {{ Request::is('lemburs') ? 'active' : '' }}" href="/lemburs">
                    <i class="bi bi-file-earmark-check-fill"></i>                    
                    <span class="nav-link-text ms-1">Data Lembur</span>
                </a>
            </li>
        </ul>
    </div>
</aside>