@extends('layouts.main_user')

@section('main')

<div class="container">
    <div class="card">
        <div class="card-body p-3">
        <div class="row">
            <div class="col-lg-12">
                <div class="d-flex flex-column h-100">
                    <h5 class="font-weight-bolder">Selamat datang di Aju Lembur App</h5>
                    <p class="mb-5">
                        Untuk pengajuan lembur, silakan isi form di menu aju lembur.
                        Untuk melihat data lembur, silakan akses menu Data Lembur. Selamat melembur!
                    </p>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
    
@endsection