<?php

use App\Http\Controllers\LemburController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\UserController;
use App\Models\Lembur;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => 'auth'], function () {
    Route::group(['middleware' => 'admin'], function () {
    });
    Route::get('/welcome', function () {
        $data = [
            'title' => 'Welcome',
            'active' => 'welcome',
            'badge' => 'Home'
        ];

        return view('welcome', $data);
    });

    Route::get('/lemburs/autocomplete', [LemburController::class, 'autocomplete'])->name('autocomplete');
    Route::put('/lemburs/{code}/reject', [LemburController::class, 'reject']);
    Route::get('/lemburs/{code}/approve', [LemburController::class, 'approve']);
    Route::resource('/lemburs', LemburController::class);

    Route::get('/logout', [LoginController::class, 'logout']);
});

Route::group(['middleware' => 'guest'], function () {
    Route::get('/', [LoginController::class, 'create'])->name('login');
    Route::post('/', [LoginController::class, 'authenticate']);

    Route::get('/register', [RegisterController::class, 'create']);
    Route::post('/register', [RegisterController::class, 'store']);
});
